from django.shortcuts import render

from MAIN_APP.models_dir.strip_model import Strip

# Create your views here.

# def index(request):
#    return render(request, 'INDEX/index.html')

# from django.contrib.auth.decorators import login_required


# @login_required
def index(request):
    abc = Strip.objects.order_by('ime')
    sqlupit = abc.query
    context1 = {'comics': abc, 'sql': sqlupit}
    return render(request, 'INDEX/INDEX.html', context1)
