from rest_framework import serializers
from django.contrib.auth.models import User

from django.contrib.auth import update_session_auth_hash


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password')

    def create(self, validated_data):   # dodaje novog usera i hashira password
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance, validated_data):     # mijenja i hashira password
        instance.username = validated_data.get('username', instance.username)
        instance.save()
        password = validated_data.get('password', None)
        instance.set_password(password)
        instance.save()
        update_session_auth_hash(self.context.get('request'), instance)
        return instance
