from rest_framework import serializers
from MAIN_APP.models_dir.universe_model import Universe


class UniverseSerializer(serializers.HyperlinkedModelSerializer):

    usr = serializers.PrimaryKeyRelatedField(read_only=True,    # snima trenutno logiranog usera u serijalizer
                                             default=serializers.CurrentUserDefault())

    class Meta:
        model = Universe
        fields = ('id', 'naziv', "usr")

    def save(self, **kwargs):   # snima trenutno logiranog usera u serijalizer
        kwargs["usr"] = self.fields["usr"].get_default()
        return super().save(**kwargs)
