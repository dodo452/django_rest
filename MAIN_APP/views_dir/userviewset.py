from rest_framework import viewsets

from MAIN_APP.serializers_dir.userserializer import UserSerializer
from django.contrib.auth.models import User


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('id')
    serializer_class = UserSerializer

    def get_queryset(self):
        aa = self.request.user.id
        queryset = User.objects.filter(id=aa).order_by('id')
        # queryset = Strip.objects.all().order_by('ime')
        return queryset
