from rest_framework import viewsets

from MAIN_APP.serializers_dir.stripserializer import StripSerializer
from MAIN_APP.models_dir.strip_model import Strip

from rest_framework.permissions import IsAuthenticated


class StripViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = StripSerializer

    def get_queryset(self):
        aa = self.request.user.id
        queryset = Strip.objects.filter(strip_id__usr=aa).order_by('id')
        # queryset = Strip.objects.all().order_by('ime')
        return queryset
