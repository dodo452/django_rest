from rest_framework import serializers
from MAIN_APP.models_dir.strip_model import Strip


class StripSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Strip
        fields = ('id', 'ime', 'pravo_ime', 'godine')
