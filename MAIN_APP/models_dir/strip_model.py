from django.db import models
from .universe_model import Universe

# Create your models here.


class Strip(models.Model):
    ime = models.CharField(max_length=20)
    pravo_ime = models.CharField(max_length=60, null=True)
    godine = models.IntegerField()
    strip = models.ForeignKey(Universe, on_delete=models.CASCADE)

    def __str__(self):
        return self.ime

    class Meta:
        verbose_name_plural = 'Stripovi'
