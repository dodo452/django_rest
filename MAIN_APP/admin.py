from django.contrib import admin

# Register your models here.

from MAIN_APP.models_dir.universe_model import Universe
from MAIN_APP.models_dir.strip_model import Strip


class StripAdmin(admin.ModelAdmin):
    list_display = ('ime', 'pravo_ime', 'strip')


class UniverseAdmin(admin.ModelAdmin):
    list_display = ('naziv', 'usr')


admin.site.register(Universe, UniverseAdmin)
admin.site.register(Strip, StripAdmin)
