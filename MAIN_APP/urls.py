from django.urls import include, path
from rest_framework import routers
from .views_dir.stripviewset import StripViewSet
from .views_dir.universeviewset import UniverseViewSet
from .views_dir.userviewset import UserViewSet

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)


router = routers.DefaultRouter()
router.register('stripovi', StripViewSet, base_name='stripovi')
router.register('svemiri', UniverseViewSet, base_name='svemiri')
router.register('korisnici', UserViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    path(r'', include(router.urls)),
    path(r'api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
    path(r'api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path(r'api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

]
