from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Universe(models.Model):
    naziv = models.CharField(max_length=20)
    usr = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.naziv

    class Meta:
        verbose_name_plural = 'Universe'
