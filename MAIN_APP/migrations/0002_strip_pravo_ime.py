# Generated by Django 2.2.4 on 2019-08-08 21:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MAIN_APP', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='strip',
            name='pravo_ime',
            field=models.CharField(max_length=60, null=True),
        ),
    ]
