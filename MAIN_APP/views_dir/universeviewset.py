from rest_framework import viewsets

from MAIN_APP.serializers_dir.universeserializer import UniverseSerializer
from MAIN_APP.models_dir.universe_model import Universe

from rest_framework.permissions import IsAuthenticated


class UniverseViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = UniverseSerializer

    def get_queryset(self):
        aa = self.request.user.id
        queryset = Universe.objects.filter(usr=aa).order_by('naziv')
        return queryset
